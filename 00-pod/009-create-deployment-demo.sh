#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

kubectl delete deployment/nginx --ignore-not-found

p '# Create deployment:'
pe "kubectl create deployment nginx --image=nginx"
p ''

p '# Show deployments:'
pe "kubectl get deployment"
p ''

p '# Show pods with labels for use with selector:'
pe "kubectl get po --show-labels"
p ''

#POD_NAME=$(k get pod -l run=nginx -o jsonpath='{.items[0].metadata.name}')

p '# Describe pod using selector:'
pe "kubectl describe po -l app=nginx"
p ''

p '# Run a bash shell in the pod'
p 'k exec -it nginx-<tab> -- /bin/bash'
p ''

p '# Teardown resources:'
pe "kubectl delete deployment/nginx"
p ''

p ''
