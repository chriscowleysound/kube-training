#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

POD_YAML=005-hostaliases-pod.yaml
POD_NAME=hostaliases-pod

kubectl delete -f $POD_YAML --ignore-not-found

p 'cat $POD_YAML'
$cat $POD_YAML
p ''

p '# Create pod'
pe "kubectl create -f $POD_YAML"
p ''

p '# Show pod logs:'
pe "kubectl logs $POD_NAME"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $POD_YAML"
p ''

p ''
