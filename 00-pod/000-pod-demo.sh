#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

POD_YAML=000-pod.yaml
POD_NAME=nginx-pod

kubectl delete -f $POD_YAML --ignore-not-found

p "cat $POD_YAML"
$cat $POD_YAML
p ''

p '# Create pod'
pe "kubectl apply -f $POD_YAML"
p ''

p '# Show pod is starting:'
pe "kubectl get po"
p ''

p '# Wait until pod is ready:'
pe "kubectl wait po $POD_NAME --for condition=ready"
p ''

p '# Show pod is Running:'
pe "kubectl get po"
p ''

p '# Check config file in the pod:'
p "kubectl exec -it $POD_NAME -- cat /etc/nginx/conf.d/default.conf"
kubectl exec -it $POD_NAME -- cat /etc/nginx/conf.d/default.conf | $cat
p ''

p '# Create a tunnel between nginx-pod:80 and locahost:8080'
p 'kubectl port-forward $POD_NAME 8080:80'
kubectl port-forward $POD_NAME 8080:80 &
sleep 2
p ''

p '# Open http://127.0.0.1:8080 in browser:'
p 'open http://127.0.0.1:8080'
sleep 5
curl http://127.0.0.1:8080/
p ''

p '# Kill port-forward'
pkill kubectl >& /dev/null
p ''

p '# Check nginx log for access:'
pe "kubectl logs $POD_NAME"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $POD_YAML"
p ''

p ''
