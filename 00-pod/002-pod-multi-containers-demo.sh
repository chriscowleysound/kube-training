#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

POD_YAML=002-pod-multi-containers.yaml
POD_NAME=multi-containers

kubectl delete -f $POD_YAML --ignore-not-found

p 'cat $POD_YAML'
$cat $POD_YAML
p ''

p '# Create pod'
pe "kubectl create -f $POD_YAML"
p ''

p '# Show pod is starting:'
pe "kubectl get po"
p ''

p '# Wait until pod is ready:'
pe "kubectl wait po $POD_NAME --for condition=ready"
p ''

p '# Show pod is Running:'
pe "kubectl get po"
p ''

p '# Connect from busybox to nginx:'
pe "kubectl exec -it $POD_NAME -c busybox -- wget -S -O - http://127.0.0.1"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $POD_YAML"
p ''

p ''
