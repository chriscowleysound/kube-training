#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

DEP_YAML=070-deployment.yaml
DEP_NAME=nginx-deployment

kubectl delete -f $DEP_YAML --ignore-not-found

p "cat $DEP_YAML"
$cat $DEP_YAML
p ''

p '# Create deployment:'
pe "kubectl apply -f $DEP_YAML --record"
p ''

p '# Show deployment status:'
pe "kubectl get deployment"
p ''

p '# Wait until deployment is available:'
pe "kubectl wait deployment $DEP_NAME --for condition=available"
p ''

p '# Show pods status:'
pe "kubectl get po"
p ''

p '# Show deployment rollout status:'
pe "kubectl rollout status deployment $DEP_NAME"
p ''

p '# Show deployment status:'
pe "kubectl get deployment $DEP_NAME"
p ''

p '# Show ReplicaSets:'
pe "kubectl get rs"
p ''

p '# Show Pods:'
pe "kubectl get po"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $DEP_YAML"
p ''

p ''
