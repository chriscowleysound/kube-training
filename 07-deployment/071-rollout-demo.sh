#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

DEP_YAML=070-deployment.yaml
DEP_NAME=nginx-deployment

kubectl delete -f $DEP_YAML --ignore-not-found

p "cat $DEP_YAML"
$cat $DEP_YAML
p ''

p '# Create deployment:'
pe "kubectl apply -f $DEP_YAML --record"
p ''

p '# Show deployment image:'
pe "kubectl describe deployment $DEP_NAME | grep -i image"
p ''

p '# Wait until deployment is available:'
pe "kubectl wait deployment $DEP_NAME --for condition=available"
p ''

p '# Change deployment container image:'
pe "kubectl set image deployment $DEP_NAME nginx=nginx:latest"
p ''

p '# Show deployment image:'
pe "kubectl describe deployment $DEP_NAME | grep -i image"
p ''

p '# Show deployment history:'
pe "kubectl rollout history deployment $DEP_NAME"
p ''

p '# Rollback deployment:'
pe "kubectl rollout undo deployment $DEP_NAME"
p ''

p '# Show deployment image:'
pe "kubectl describe deployment $DEP_NAME | grep -i image"
p ''

p '# Show deployment history:'
pe "kubectl rollout history deployment $DEP_NAME"
p ''

p '# Show revision details:'
pe "kubectl rollout history deployment $DEP_NAME --revision=2"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $DEP_YAML"
p ''

p ''
