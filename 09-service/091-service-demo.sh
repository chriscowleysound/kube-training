#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

DEP_YAML=090-echo-deployment.yaml
DEP_NAME=echo-deployment

SVC_YAML=091-service.yaml
SVC_NAME=echo-service

kubectl delete -f $DEP_YAML --ignore-not-found
#kubectl delete -f $SVC_YAML --ignore-not-found

p '# Create deployment:'
pe "kubectl apply -f $DEP_YAML"
p ''

p "cat $SVC_YAML"
$cat $SVC_YAML
p ''

p '# Create svc:'
pe "kubectl apply -f $SVC_YAML"
p ''

p '# Wait until service external-ip is ready:'
pe "kubectl get svc"
p ''

p '# Show endpoints:'
pe "kubectl get ep"
p ''

p '# Describe service'
pe "kubectl describe svc echo-service | grep Ingress"
p ''

p '# curl <ip>'
p ''

p '# Teardown resources:'
pe "kubectl delete -f $DEP_YAML"
pe "kubectl delete -f $SVC_YAML"
p ''

p ''
