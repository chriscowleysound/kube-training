#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

DEP_YAML=070-deployment.yaml
DEP_NAME=nginx-deployment

kubectl delete -f $DEP_YAML --ignore-not-found

p "cat $DEP_YAML"
$cat $DEP_YAML
p ''

p '# Create deployment:'
pe "kubectl apply -f $DEP_YAML"
p ''

p '# Show deployment replicas:'
pe "kubectl describe deployment $DEP_NAME | grep -i replicas"
p ''

p '# Wait until deployment is available:'
pe "kubectl wait deployment $DEP_NAME --for condition=available"
p ''

p '# Change deployment replicas:'
pe "kubectl scale deployment $DEP_NAME --replicas=5"
p ''

p '# Show deployment replicas:'
pe "kubectl describe deployment $DEP_NAME | grep -i replicas"
p ''

p '# Show pods:'
pe "kubectl get po"
p ''

p '# Apply deployment:'
pe "kubectl apply -f $DEP_YAML --record"
p ''

p '# Show deployment replicas:'
pe "kubectl describe deployment $DEP_NAME | grep -i replicas"
p ''

p '# Show pods:'
pe "kubectl get po"
p ''

p '# Delete pods:'
pe "kubectl delete po -l app=nginx"
p ''

p '# Show pods:'
pe "kubectl get po"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $DEP_YAML"
p ''

p ''
