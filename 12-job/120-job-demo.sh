#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

JOB_YAML=120-job.yaml
JOB_NAME=wait

kubectl delete -f $JOB_YAML --ignore-not-found

p "cat $JOB_YAML"
$cat $JOB_YAML
p ''

p '# Create deployment:'
pe "kubectl apply -f $JOB_YAML"
p ''

p '# Show job'
pe "kubectl describe job $JOB_NAME"
p ''

p '# Show pod:'
pe "kubectl describe po -l job-name=$JOB_NAME"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $JOB_YAML"
p ''

p ''
