#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

POD_YAML=050-command-liveness.yaml
export POD_NAME=liveness-exec
WAIT=35

kubectl delete -f $POD_YAML --ignore-not-found

p 'cat $POD_YAML'
$cat $POD_YAML
p ''

p '# Create pod:'
pe "kubectl apply -f $POD_YAML"
p ''

p '# Show liveness command:'
pe "kubectl describe po $POD_NAME | grep -i Liveness:"
p ''

p '# Wait until pod is ready:'
pe "kubectl wait po $POD_NAME --for condition=ready"
p ''

p '# Watch for RESTARTS #'
pe "kubectl get po $POD_NAME"
p ''

sleep 60

p '# Watch for RESTARTS #'
pe "kubectl get po $POD_NAME"
p ''

p '# Show pod is unhealthy:'
pe "kubectl describe po $POD_NAME | grep -i unhealthy"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $POD_YAML"
p ''

p ''
