#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

DEP_YAML=090-echo-deployment.yaml
DEP_NAME=echo-deployment

kubectl delete -f $DEP_YAML --ignore-not-found

p "cat $DEP_YAML"
$cat $DEP_YAML
p ''

p '# Create deployment:'
pe "kubectl apply -f $DEP_YAML"
p ''

p '# Show deployment'
pe "kubectl describe deployment"
p ''

p '# Show pod:'
pe "kubectl get po"
p ''

p '# Show pod Label'
pe "kubectl describe po | grep Label"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $DEP_YAML"
p ''

p ''
