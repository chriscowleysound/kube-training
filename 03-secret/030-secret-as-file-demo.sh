#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

SECRET_NAME=mysecret
SECRET_FILE=mysecret.txt

kubectl delete secret $SECRET_NAME --ignore-not-found

p '# Create secret file'
pe "echo -n mysecret > $SECRET_FILE"
p ''

p '# Create secret'
pe "kubectl create secret generic $SECRET_NAME --from-file=password=$SECRET_FILE"
p ''

POD_YAML=030-secret-as-file.yaml
POD_NAME=secret-file-pod

kubectl delete -f $POD_YAML --ignore-not-found

p 'cat $POD_YAML'
$cat $POD_YAML
p ''

p '# Create pod'
pe "kubectl apply -f $POD_YAML"
p ''

p '# Show pod is starting:'
pe "kubectl get po"
p ''

p '# Wait until pod is ready:'
pe "kubectl wait po $POD_NAME --for condition=ready"
p ''

p '# Show pod is Running:'
pe "kubectl get po"
p ''

p '# Check secret in the pod:'
pe "kubectl exec -it $POD_NAME -- cat /etc/mysecret/password && echo"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $POD_YAML"
pe "kubectl delete secret $SECRET_NAME"
pe "rm -f $SECRET_FILE"
p ''

p ''
