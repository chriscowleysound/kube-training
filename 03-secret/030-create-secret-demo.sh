#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

SECRET_NAME=mysecret
SECRET_FILE=mysecret.txt

kubectl delete secret $SECRET_NAME --ignore-not-found

p '# Create secret file'
pe "echo -n mysecret > $SECRET_FILE"
p ''

p '# Create secret'
pe "kubectl create secret generic $SECRET_NAME --from-file=password=$SECRET_FILE"
p ''

p '# Show secrets'
pe "kubectl get secret"
p ''

p '# Describe secret'
pe "kubectl describe secret $SECRET_NAME"
p ''

p '# Get secret yaml'
pe "kubectl get secret $SECRET_NAME -o yaml"
p ''

p '# Decode secret'
pe "echo 'bXlzZWNyZXQ=' | base64 -D && echo"
p ''

p '# Teardown resources:'
pe "kubectl delete secret $SECRET_NAME"
pe "rm -f $SECRET_FILE"
p ''

p ''
