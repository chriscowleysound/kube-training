#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

CM_NAME=log-config
POD_YAML=020-configmap-pod.yaml
POD_NAME=configmap-pod

kubectl delete cm $CM_NAME --ignore-not-found
kubectl delete -f $POD_YAML --ignore-not-found

p '# Create configmap'
pe "kubectl create configmap $CM_NAME --from-literal=log_conf_key=log_level:high"
p ''

p '# Show configmap:'
pe "kubectl get cm $CM_NAME -o yaml"
p ''

p "cat $POD_YAML"
$cat $POD_YAML
p ''

p '# Create pod:'
pe "kubectl apply -f $POD_YAML"
p ''

p '# Show pod is starting:'
pe "kubectl get po"
p ''

p '# Wait until pod is ready:'
pe "kubectl wait po $POD_NAME --for condition=ready"
p ''

p '# Show pod is Running:'
pe "kubectl get po"
p ''

p '# Check config file in the pod:'
pe "kubectl exec -it $POD_NAME -- cat /etc/config/log.conf && echo"
p ''

p '# Check nginx log for access:'
pe "kubectl logs $POD_NAME"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $POD_YAML"
pe "kubectl delete configmap $CM_NAME"
p ''

p ''
