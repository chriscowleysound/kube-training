#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

CRONJOB_YAML=130-cronjob.yaml
CRONJOB_NAME=hello
SELECTOR=app=hello-cronpod

kubectl delete -f $CRONJOB_YAML --ignore-not-found

p "cat $CRONJOB_YAML"
$cat $CRONJOB_YAML
p ''

p '# Create deployment:'
pe "kubectl apply -f $CRONJOB_YAML"
p ''

p '# Show cronjob'
pe "kubectl get cronjob -o yaml"
p ''

sleep 60

p '# Show job:'
pe "kubectl get job -l $SELECTOR"
p ''

p '# Show pod:'
pe "kubectl describe po -l $SELECTOR"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $CRONJOB_YAML"
p ''

p ''
