#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

DEP_YAML=070-deployment.yaml
DEP_NAME=nginx-deployment
SVC_NAME=nginx
HPA_YAML=075-deployment-hpa.yaml
HPA_NAME=nginx
POD_NAME=load

kubectl delete -f $DEP_YAML --ignore-not-found
kubectl delete -f $HPA_YAML --ignore-not-found
kubectl delete svc $SVC_NAME --ignore-not-found
kubectl delete pod $POD_NAME --ignore-not-found

p "cat $HPA_YAML"
$cat $HPA_YAML
p ''

p '# Create deployment:'
pe "kubectl apply -f $DEP_YAML"
p ''

p '# Create service:'
pe "kubectl expose deployment $DEP_NAME --name nginx"
p ''

p '# Create hpa:'
pe "kubectl apply -f $HPA_YAML"
#pe "kubectl autoscale deployment $DEP_NAME --min=2 --max=5 --cpu-percent=20"
p ''

p '# Wait until deployment is available:'
pe "kubectl wait deployment $DEP_NAME --for condition=available"
p ''

p '# Run some load to trigger hpa:'
pe "kubectl apply -f load-generator.yaml"
p ''

sleep 30

p '# Get hpa:'
pe "kubectl get hpa"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $DEP_YAML"
pe "kubectl delete -f $HPA_YAML"
pe "kubectl delete svc $SVC_NAME"
pe "kubectl delete pod $POD_NAME"
p ''

p ''
