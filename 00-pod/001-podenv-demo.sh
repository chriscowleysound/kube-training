#!/usr/bin/env bash
set -euo pipefail
. ../demo-wrapper.sh

POD_YAML=001-podenv.yaml
POD_NAME=podenv

kubectl delete -f $POD_YAML --ignore-not-found

p 'cat $POD_YAML'
$cat $POD_YAML
p ''

p '# Create podenv'
pe "kubectl apply -f $POD_YAML"
p ''

p '# Show pod is starting:'
pe "kubectl get po"
p ''

p '# Wait until pod is ready:'
pe "kubectl wait po $POD_NAME --for condition=ready"
p ''

p '# Show pod is Running:'
pe "kubectl get po"
p ''

p '# Show pod env variables'
pe "kubectl exec -it $POD_NAME -- env | grep ENV_VAR_"
p ''

p '# Teardown resources:'
pe "kubectl delete -f $POD_YAML"
p ''

p ''
